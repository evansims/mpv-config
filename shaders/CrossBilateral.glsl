// KrigBilateral by Shiandow
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3.0 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library.


//!HOOK CHROMA
//!BIND HOOKED
//!BIND LUMA
//!SAVE DOWNSCALEDLUMAX
//!HEIGHT LUMA.h
//!WHEN CHROMA.w LUMA.w <

// -- Downscaling --
#define offset   (-vec2(0.0, 0.0)*LUMA_size*CHROMA_pt)

#define dxdy     (vec2(CHROMA_pt.x, LUMA_pt.y))
#define ddxddy   (LUMA_pt)

#define factor   ((ddxddy*vec2(CHROMA_size.x, LUMA_size.y))[axis])

#define axis 0

#define Kernel(x) clamp(0.5 + (0.5 - abs(x)) / factor, 0.0, 1.0)
#define taps (1.0 + factor)

vec4 hook() {
    // Calculate bounds
    float low  = floor((LUMA_pos - 0.5*taps*dxdy) * LUMA_size - offset + 0.5)[axis];
    float high = floor((LUMA_pos + 0.5*taps*dxdy) * LUMA_size - offset + 0.5)[axis];

    float W = 0.0;
    vec4 avg = vec4(0);
    vec2 pos = LUMA_pos;

    for (float k = 0.0; k < high - low; k++) {
        pos[axis] = ddxddy[axis] * (k + low + 0.5);
        float rel = (pos[axis] - LUMA_pos[axis])*CHROMA_size[axis] + offset[axis]*factor;
        float w = Kernel(rel);

        vec4 y = textureLod(LUMA_raw, pos, 0.0).xxxx;
        y.y *= y.y;
        avg += w * y;
        W += w;
    }
    avg /= vec4(W);

    return avg;
}

//!HOOK CHROMA
//!BIND HOOKED
//!BIND DOWNSCALEDLUMAX
//!SAVE LOWRES_YUV
//!COMPONENTS 4

// -- Downscaling --
#define offset   (-vec2(0.0, 0.0)*DOWNSCALEDLUMAX_size*CHROMA_pt)

#define dxdy     (CHROMA_pt)
#define ddxddy   (DOWNSCALEDLUMAX_pt)

#define factor   ((ddxddy*CHROMA_size)[axis])

#define axis 1

#define Kernel(x) clamp(0.5 + (0.5 - abs(x)) / factor, 0.0, 1.0)
#define taps (1.0 + factor)

vec4 hook() {
    // Calculate bounds
    float low  = floor((DOWNSCALEDLUMAX_pos - 0.5*taps*dxdy) * DOWNSCALEDLUMAX_size - offset + 0.5)[axis];
    float high = floor((DOWNSCALEDLUMAX_pos + 0.5*taps*dxdy) * DOWNSCALEDLUMAX_size - offset + 0.5)[axis];

    float W = 0.0;
    vec2 avg = vec2(0);
    vec2 pos = DOWNSCALEDLUMAX_pos;

    for (float k = 0.0; k < high - low; k++) {
        pos[axis] = ddxddy[axis] * (k + low + 0.5);
        float rel = (pos[axis] - DOWNSCALEDLUMAX_pos[axis])*CHROMA_size[axis] + offset[axis]*factor;
        float w = Kernel(rel);

        avg += w * textureLod(DOWNSCALEDLUMAX_raw, pos, 0.0).xy;
        W += w;
    }
    avg /= vec2(W);

    return vec4(avg, CHROMA_tex(CHROMA_pos).xy);
}

//!HOOK CHROMA
//!BIND HOOKED
//!BIND LUMA
//!BIND LOWRES_YUV
//!WIDTH LUMA.w
//!HEIGHT LUMA.h

// -- KrigBilateral --

// -- Convenience --
#define sqr(x)   dot(x,x)
#define bitnoise 1.0/(2.0*255.0)
#define noise    0.05//5.0*bitnoise
#define chromaOffset vec2(0.0, 0.0)

// -- Window Size --
#define taps 3
#define even (float(taps) - 2.0 * floor(float(taps) / 2.0) == 0.0)
#define minX int(1.0-ceil(float(taps)/2.0))
#define maxX int(floor(float(taps)/2.0))

// -- Input processing --
// Luma value
//#define GetLuma(x,y)   LOWRES_YUV_tex(HOOKED_pos + NATIVE_pt*vec2(x,y))[0]
// Chroma value
#define GetChroma(x,y) LOWRES_YUV_tex(LOWRES_YUV_pt*(pos+vec2(x,y)+vec2(0.5)))

#define N (taps*taps - 1)

#define M(i,j) Mx[min(i,j)*N + max(i,j) - min(i,j)*(min(i,j)+1)/2]

#define C(i,j) (1.0 / (1.0 + (sqr(X[i].x - X[j].x)/localVar + sqr((coords[i] - coords[j])/radius))))
#define c(i) (1.0 / (1.0 + (sqr(X[i].x - y)/localVar + sqr((coords[i] - offset)/radius))))

#define f1(i) {b[i] = c(i) - c(N) - ( C(i,N) - C(N,N) ); \
        for (int j=i; j<N; j++) M(i, j) = C(i,j) - C(j,N) - ( C(i,N) - C(N,N) );}

#define f2(i) for (int j=i+1; j<N; j++) {b[j] -= b[i] * M(j, i) / M(i, i); \
        for (int k=j; k<N; k++) M(j, k) -= M(i, k) * M(j, i) / M(i, i);}

#define I2(f, n) f(n) f(n+1)
#define I4(f, n) I2(f, n) I2(f, n+2)
#define I8(f, n) I4(f, n) I4(f, n+4)

vec4 hook() {
    float y = LUMA_tex(LUMA_pos).x;

    // Calculate position
    vec2 pos = LUMA_pos * LOWRES_YUV_size - chromaOffset - vec2(0.5);
    vec2 offset = pos - (even ? floor(pos) : round(pos));
    pos -= offset;

    vec2 coords[N+1];
    vec4 X[N+1];
    int i=0;
    for (int xx = minX; xx <= maxX; xx++)
    for (int yy = minX; yy <= maxX; yy++)
        if (!(xx == 0 && yy == 0)) {
            coords[i] = vec2(xx,yy);
            X[i++] = GetChroma(xx, yy);
        }

    coords[N] = vec2(0,0);
    X[N] = GetChroma(0,0);

    vec4 total = vec4(0);
    for (int i=0; i<N+1; i++) {
        vec2 w = clamp(vec2(2.0) - abs(coords[i] - offset), 0.0, 1.0);
        total += w.x*w.y*vec4(X[i].x, X[i].x*X[i].x, X[i].y - X[i].x*X[i].x, 1.0);
    }
    total /= 2.0*total.w;
    float localVar = sqr(noise) + total.y - total.x*total.x + sqr(y - total.x) + total.z;
    float radius = mix(1.0, 1.8, (localVar - total.z) / localVar);

    float Mx[N*(N+1)/2];
    float b[N];

    I8(f1, 0)

    I8(f2, 0)

    vec4 interp = X[N];
    for (int i=N-1; i>=0; i--) {
        for (int j=i+1; j<N; j++) {
            b[i] -= M(i, j) * b[j];
        }
        b[i] /= M(i, i);
        interp += b[i] * (X[i] - X[N]);
    }

    return interp.zwxx;
}
